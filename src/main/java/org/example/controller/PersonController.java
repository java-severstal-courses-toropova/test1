package org.example.controller;

import jakarta.validation.Valid;
import org.example.dao.PersonDao;
import org.example.dto.Person;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/persons")
public class PersonController {
    private final PersonDao personDao;

    public PersonController(PersonDao personDao) {
        this.personDao = personDao;
    }

    @PostMapping
    public ResponseEntity<Object> save(@Valid @RequestBody Person person, BindingResult result) {
        if (result.hasErrors()) {
            List<ObjectError> errors = result.getAllErrors();
             String errorMessage = errors.stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining("\r\n"));
            return ResponseEntity.badRequest().body(errorMessage);
        } else {
            Person createdPerson = personDao.save(person);
            return ResponseEntity.ok(createdPerson);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        personDao.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> findById(@PathVariable Long id) {
        Person person = personDao.findById(id);
        if (person!=null){
            return ResponseEntity.ok(person);
        }
        return ResponseEntity.badRequest().body("no person with this id");
    }

    @GetMapping("/all")
    public ResponseEntity<List<Person>> findAll() {
        List<Person> persons = personDao.findAll();
        return ResponseEntity.ok(persons);
    }

//    @GetMapping
//    public ResponseEntity<List<Person>> findByFirstNameAndLastName(@RequestParam String firstName, @RequestParam String lastName) {
//        List<Person> persons = personService.findByFirstNameAndLastName(firstName, lastName);
//        return ResponseEntity.ok(persons);
//    }

    @GetMapping("/search/{searchTerm}")
    public ResponseEntity<List<Person>> search(@PathVariable String searchTerm) {
        List<Person> persons = personDao.search(searchTerm);
        return ResponseEntity.ok(persons);
    }
}
