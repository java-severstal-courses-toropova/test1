package org.example.dao;

import com.querydsl.core.types.Predicate;
import org.example.dto.Person;

import java.util.List;

public interface PersonDao {

    Person findById(long id);

    List<Person> findAll(Predicate... where);

    Person save(Person p);

    long count();

    void delete(long id);

    List<Person> search(String searchString);

}
