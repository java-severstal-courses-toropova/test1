package org.example.dao;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.QBean;
import com.querydsl.sql.SQLQueryFactory;
import org.example.dto.Person;
import org.example.events.PersonEvent;
import org.example.service.LogEventsService;
import org.example.utils.Operations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

import static com.querydsl.core.types.Projections.bean;
import static org.example.entities.QPerson.person;

@Transactional
@Service
public class PersonDaoImpl implements PersonDao {
    private final LogEventsService logEventsService;

    @Autowired
    SQLQueryFactory queryFactory;

    @Autowired
    private DataSource dataSource;


    final QBean<Person> personBean = bean(Person.class, person.id, person.firstname, person.lastname, person.phone, person.email, person.personalDescription);

    public PersonDaoImpl(LogEventsService logEventsService) {
        this.logEventsService = logEventsService;
    }

    @Override
    public Person findById(long id) {
        return queryFactory.select(personBean)
                .from(person)
                .where(person.id.eq(id))
                .fetchOne();
    }

    @Override
    public List<Person> findAll(Predicate... where) {
        return queryFactory.select(personBean)
                .from(person)
                .where(where)
                .fetch();
    }

    @Override
    public Person save(Person p) {
        Long id = p.getId();

        if (id == null) {
            id = queryFactory.insert(person)
                    .populate(p)
                    .executeWithKey(person.id);
            p.setId(id);
            logEventsService.publishEvent(new PersonEvent(p, Operations.CREATED));
        } else {
            queryFactory.update(person)
                    .populate(p)
                    .where(person.id.eq(id)).execute();
            logEventsService.publishEvent(new PersonEvent(p, Operations.UPDATED));
        }

        return p;
    }

    @Override
    public long count() {
        return queryFactory.from(person).fetchCount();
    }

    @Override
    public void delete(long id) {
        Person person1 = queryFactory.select(personBean)
                .from(person)
                .where(person.id.eq(id))
                .fetchOne();
        queryFactory.delete(person)
                .where(person.id.eq(id))
                .execute();
        logEventsService.publishEvent(new PersonEvent(person1, Operations.DELETED));
    }

    @Override
    public List<Person> search(String searchString) {
//        BooleanBuilder builder = new BooleanBuilder();
//        builder.or(person.firstname.contains(searchString));
//        builder.or(person.lastname.contains(searchString));
//        builder.or(person.email.contains(searchString));
//        builder.or(person.phone.contains(searchString));
//        return queryFactory.select(personBean)
//                .from(person)
//                .where(builder)
//                .fetch();
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        String sql = "SELECT * FROM person WHERE textsearchable_index_col @@ to_tsquery('english', :search_query)";
        return jdbcTemplate.query(sql,new MapSqlParameterSource("search_query", searchString),
                new BeanPropertyRowMapper<>(Person.class));

    }

}
