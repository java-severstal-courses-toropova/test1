package org.example.entities;

import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.BeanPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.RelationalPathBase;

//@Table("PERSON")
public class QPerson extends RelationalPathBase<QPerson> {

    public static final QPerson person = new QPerson("person");

    public final StringPath firstname = createString("firstname");

    public final StringPath lastname = createString("lastname");

    public final StringPath email = createString("email");
    public final StringPath phone = createString("phone");

    public final StringPath personalDescription = createString("personalDescription");

    public final NumberPath<Long> id = createNumber("id", Long.class);


    public QPerson(String variable) {
        super(QPerson.class, PathMetadataFactory.forVariable(variable), "", "person");
        addMetadata();
    }

    public QPerson(BeanPath<? extends QPerson> entity) {
        super(entity.getType(), entity.getMetadata(), "", "person");
        addMetadata();
    }

    public QPerson(PathMetadata metadata) {
        super(QPerson.class, metadata, "", "person");
        addMetadata();
    }

    protected void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id"));
        addMetadata(firstname, ColumnMetadata.named("firstname"));
        addMetadata(lastname, ColumnMetadata.named("lastname"));
        addMetadata(email, ColumnMetadata.named("email"));
        addMetadata(phone, ColumnMetadata.named("phone"));
        addMetadata(personalDescription, ColumnMetadata.named("personal_description"));
    }

}