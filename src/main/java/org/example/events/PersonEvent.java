package org.example.events;

import lombok.Getter;
import org.example.dto.Person;
import org.example.utils.Operations;

@Getter
public class PersonEvent {
    private final Person person;
    private final Operations operation;

    public PersonEvent(Person person, Operations operation) {
        this.person = person;
        this.operation = operation;
    }

}
