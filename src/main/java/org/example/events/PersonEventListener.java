package org.example.events;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class PersonEventListener {

    @Autowired
    private DataSource dataSource;

    @EventListener
    public void handlePersonEvent(PersonEvent event) {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("event", ow.writeValueAsString(event.getPerson()));
            parameters.put("description", event.getOperation());
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource)
                    .withTableName("events")
                    .usingGeneratedKeyColumns("id", "created_at");
            simpleJdbcInsert.execute(parameters);
            log.info("Person {}: {}", event.getOperation(), event.getPerson());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
