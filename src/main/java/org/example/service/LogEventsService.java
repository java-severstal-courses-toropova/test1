package org.example.service;

import org.example.events.PersonEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;

@Service
public class LogEventsService implements ApplicationEventPublisherAware {
    ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public void publishEvent(PersonEvent personEvent) {
        applicationEventPublisher.publishEvent(personEvent);
    }
}
