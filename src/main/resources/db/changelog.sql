-- ChangeSet 1
CREATE TABLE person
(
    id        SERIAL PRIMARY KEY NOT NULL,
    firstname VARCHAR(50),
    lastname  VARCHAR(50),
    email     VARCHAR(50) UNIQUE,
    phone     VARCHAR(20) UNIQUE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
CREATE TABLE events
(
    id          SERIAL PRIMARY KEY NOT NULL,
    event       jsonb,
    description VARCHAR(50),
    created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


-- ChangeSet 2
ALTER TABLE person
    ADD COLUMN personal_description VARCHAR(500);
CREATE INDEX ON person
    USING GIN (
               to_tsvector('english', firstname || ' ' || lastname || ' ' || personal_description)
        );

-- ChangeSet 3
ALTER TABLE person ADD COLUMN textsearchable_index_col tsvector;
UPDATE person SET textsearchable_index_col =
                     to_tsvector('english', coalesce(firstname,'') || ' ' || coalesce(lastname,'') || ' ' || coalesce(personal_description,'') || ' ' || coalesce(phone,'') || ' ' || coalesce(email,'') );
CREATE INDEX textsearch_idx ON person USING GIN (textsearchable_index_col);
CREATE TRIGGER tsvectorupdate BEFORE INSERT OR UPDATE
    ON person FOR EACH ROW EXECUTE FUNCTION
    tsvector_update_trigger(textsearchable_index_col, 'pg_catalog.english', firstname, lastname, email, phone, person.personal_description );