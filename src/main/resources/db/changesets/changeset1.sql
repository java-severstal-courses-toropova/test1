-- ChangeSet 1
CREATE TABLE person
(
    id        SERIAL PRIMARY KEY NOT NULL,
    firstname VARCHAR(50),
    lastname  VARCHAR(50),
    email     VARCHAR(50) UNIQUE,
    phone     VARCHAR(20) UNIQUE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
CREATE TABLE events
(
    id          SERIAL PRIMARY KEY NOT NULL,
    event       jsonb,
    description VARCHAR(50),
    created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);