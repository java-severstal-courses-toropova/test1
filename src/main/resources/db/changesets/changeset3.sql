-- ChangeSet 3
ALTER TABLE person
    ADD COLUMN textsearchable_index_col tsvector;
UPDATE person
SET textsearchable_index_col =
        to_tsvector('english', firstname || ' ' || lastname || ' ' ||
                               personal_description || ' ' || phone || ' ' ||
                               email);
CREATE INDEX textsearch_idx ON person USING GIN (textsearchable_index_col);
