CREATE TRIGGER tsvectorupdate
    BEFORE INSERT OR
UPDATE
    ON person FOR EACH ROW EXECUTE FUNCTION
    tsvector_update_trigger(textsearchable_index_col, 'pg_catalog.english', firstname, lastname, email, phone, personal_description );