package org.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.example.dao.PersonDao;
import org.example.dto.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.jupiter.api.Assertions.assertEquals;


@WebMvcTest(PersonController.class)
class PersonControllerTest {
    @Autowired
    private MockMvc mvc;

    private final String API_URL = "/api/persons";

    @MockBean
    private PersonDao personDao;

    @InjectMocks
    private PersonController personController;

    Person person1 = new Person(1L,"Andrey", "Andreev", "andreev@test.ru", "1111111", "person1");
    Person person2 = new Person(2L,"Anna", "Ivanova", "ivanova@test.ru", "2222222", "person2");

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        //personDao.save(person1);
        //personDao.save(person2);
    }

//    @Test
//    void create_person() throws Exception {
//        //given(this.personDao.save(any(Person.class))).willReturn();
//        //ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
//        //String requestJson = ow.writeValueAsString(newPerson);
//        mvc.perform(post(controllerUrl).contentType(APPLICATION_JSON)
//                        .content("{\"id\":\"null\",\"firstname\":\"Ivan\", \"lastname\":\"Ivanov\", \"email\":\"ivanov@test.ru\", \"phone\":\"3333333\" }"))
//                .andExpect(status().isOk())
//        .andExpect(content().json("{}"));
//    }

    @Test
    void create_person_with_wrong_email() throws Exception {
        Person newPerson = new Person(null, "Ivan", "Ivanov", "ivanov@test", "3333333", "person3");
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(newPerson);
        mvc.perform(post(API_URL).contentType(APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("email is wrong"));
    }

    @Test
    void create_person_with_wrong_phone() throws Exception {
        Person newPerson = new Person(null, "Ivan", "Ivanov", "ivanov@test.ru", "g333333", "person3");
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(newPerson);
        mvc.perform(post(API_URL).contentType(APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("phone is wrong"));
    }

    @Test
    void save() {
    }

    @Test
    void delete() {
    }

    @Test
    void find_person_by_non_existent_id() throws Exception {
        mvc.perform(get(API_URL+"/100"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("no person with this id"));

    }

    @Test
    void find_person_by_id_1() throws Exception {
        given(this.personDao.findById(1))
        	.willReturn(person1);

        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String responseJson = ow.writeValueAsString(person1);
        mvc.perform(get(API_URL+"/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(responseJson));

    }

    @Test
    void findAll() throws Exception {
        given(this.personDao.findAll())
                .willReturn(List.of(person1,person2));
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String responseJson = ow.writeValueAsString(List.of(person1,person2));

        mvc.perform(get(API_URL+"/all"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
        .andExpect(content().json(responseJson));
    }

    @Test
    void search() {
    }

    @Test
    void testSaveWithValidPerson() {
        Person person = new Person();
        BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(false);
        when(personDao.save(person)).thenReturn(person);

        ResponseEntity<Object> response = personController.save(person, result);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(person, response.getBody());
        verify(personDao, times(1)).save(person);
    }

    @Test
    void testSaveWithInvalidPerson() {
        Person person = new Person();
        BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(true);
        List<ObjectError> errors = new ArrayList<>();
        errors.add(new ObjectError("field", "error message"));
        when(result.getAllErrors()).thenReturn(errors);

        ResponseEntity<Object> response = personController.save(person, result);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("error message", response.getBody());
        verify(personDao, never()).save(person);
    }

    @Test
    void testDeletePerson() {
        long id = 1L;
        personController.delete(id);
        verify(personDao, times(1)).delete(id);
    }

    @Test
    void testFindByIdWithExistingPerson() {
        long id = 1L;
        Person person = new Person();
        when(personDao.findById(id)).thenReturn(person);

        ResponseEntity<Object> response = personController.findById(id);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(person, response.getBody());
    }

    @Test
    void testFindByIdWithNonExistingPerson() {
        long id = 1L;
        when(personDao.findById(id)).thenReturn(null);

        ResponseEntity<Object> response = personController.findById(id);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("no person with this id", response.getBody());
    }

    @Test
    void testFindAllPersons() {
        List<Person> persons = new ArrayList<>();
        persons.add(new Person());
        persons.add(new Person());
        when(personDao.findAll()).thenReturn(persons);

        ResponseEntity<List<Person>> response = personController.findAll();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(persons, response.getBody());
    }

    @Test
    void testSearchPersons() {
        String searchTerm = "John";
        List<Person> persons = new ArrayList<>();
        persons.add(person1);
        when(personDao.search(searchTerm)).thenReturn(persons);

        ResponseEntity<List<Person>> response = personController.search(searchTerm);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(persons, response.getBody());
    }
}